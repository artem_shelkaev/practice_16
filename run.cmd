@echo off
cd %CD%

java -XX:+PrintCompilation -Dfile.encoding=UTF-8 -classpath ./target/classes  com.Main > log_example_1.log

java -XX:+PrintCompilation -XX:+UnlockDiagnosticVMOptions -XX:+PrintInlining^
 -Dfile.encoding=UTF-8 -classpath ./target/classes  com.Main > log_example_2.log
 
java -XX:+PrintCompilation -XX:+UnlockDiagnosticVMOptions -XX:+PrintIntrinsics^
 -Dfile.encoding=UTF-8 -classpath ./target/classes  com.Main > log_example_3.log
 
java -XX:+PrintCompilation -XX:+UnlockDiagnosticVMOptions -XX:+LogCompilation^
 -Dfile.encoding=UTF-8 -classpath ./target/classes  com.Main > log_example_4.log
 
pause