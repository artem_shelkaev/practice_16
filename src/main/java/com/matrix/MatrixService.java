package com.matrix;

import com.service.LineSummatorCallable;
import com.service.LineSummatorRunnable;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MatrixService {
    private BlockingQueue<int[]> taskQueue = null;
    private CopyOnWriteArrayList<Integer> listSumLine = null;

    public MatrixService() {
        this.taskQueue = new LinkedBlockingQueue<>();
        this.listSumLine = new CopyOnWriteArrayList<>();
    }


    public int sum(int[][] matrix, int numberThreads) {
        int sum = 0;
        ExecutorService executor = Executors.newFixedThreadPool(numberThreads);
        for (int[] line : matrix) {
            try {
                sum += executor.submit(new LineSummatorCallable(line)).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdownNow();
        return sum;
    }

    private void setQueue(int[][] matrix) {
        for (int[] line : matrix) {
            this.taskQueue.offer(line);
        }
    }

    private void startNewSumThreadAndNotify(AtomicInteger sum) {
        new Thread(() -> {
            synchronized (taskQueue) {
                listSumLine.stream().forEach(i -> sum.addAndGet(i));
                taskQueue.notifyAll();
            }
        }).start();
    }

    private int waitToReturnSum(AtomicInteger sum) {
        synchronized (taskQueue) {
            try {
                taskQueue.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return sum.get();
        }
    }
}

