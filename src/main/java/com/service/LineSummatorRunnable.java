package com.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.IntStream;

public class LineSummatorRunnable implements Runnable {
    private Thread thread;
    private BlockingQueue<int[]> taskQueue;
    private CopyOnWriteArrayList<Integer> list;
    private boolean isFinished;

    public LineSummatorRunnable(BlockingQueue<int[]> taskQueue, CopyOnWriteArrayList<Integer> list) {
        this.taskQueue = taskQueue;
        this.list = list;
    }

    @Override
    public void run() {
        this.thread = Thread.currentThread();
        while (!isFinished) {
            try {
                synchronized (taskQueue) {
                    if (taskQueue.size() == 0) {
                        thread.interrupt();
                        isFinished = true;
                    } else {
                        list.add(IntStream.of(taskQueue.take()).sum());
                    }
                }
            } catch (InterruptedException e) {
                isFinished = true;
                thread.interrupt();
                e.printStackTrace();
            }
        }
    }
}
