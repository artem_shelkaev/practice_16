package com.service;

import java.util.concurrent.Callable;
import java.util.stream.IntStream;

public class LineSummatorCallable implements Callable<Integer> {
    private int[] line;

    public LineSummatorCallable(int[] line) {
        this.line = line;
    }

    @Override
    public Integer call() throws Exception {
        return IntStream.of(line).sum();

    }
}
