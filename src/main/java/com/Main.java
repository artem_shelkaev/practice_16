package com;


import com.matrix.MatrixService;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                new int[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                new int[]{21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
                new int[]{31, 32, 33, 34, 35, 36, 37, 38, 39, 40},
                new int[]{41, 42, 43, 44, 45, 46, 47, 48, 49, 50},
                new int[]{51, 52, 53, 54, 55, 56, 57, 58, 59, 60}
        };

        int[][] matrix1 = new int[][]{
                new int[]{1, 2, 3},
                new int[]{4, 5, 6},
        };

        int result = 0;
        String str = "";
        Map<String,Integer> map = new HashMap<>();
        for(int i = 0; i < 10000; i++) {
            MatrixService matrixService = new MatrixService();
            result += matrixService.sum(matrix, 5);
            str += result;
            map.put(str, result);
            System.out.println(result);
        }

    }

}
